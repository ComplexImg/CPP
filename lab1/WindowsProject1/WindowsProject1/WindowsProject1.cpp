// WindowsProject1.cpp: определяет точку входа для приложения.
//

#include "stdafx.h"
#include "WindowsProject1.h"
#pragma comment(lib,"Msimg32.lib")
#include <stdio.h>

#define MAX_LOADSTRING 100

// Глобальные переменные:
HINSTANCE hInst;                                // текущий экземпляр
WCHAR szTitle[MAX_LOADSTRING];                  // Текст строки заголовка
WCHAR szWindowClass[MAX_LOADSTRING];            // имя класса главного окна

												// Отправить объявления функций, включенных в этот модуль кода:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
	_In_opt_ HINSTANCE hPrevInstance,
	_In_ LPWSTR    lpCmdLine,
	_In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

	// TODO: разместите код здесь.

	// Инициализация глобальных строк
	LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadStringW(hInstance, IDC_WINDOWSPROJECT1, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Выполнить инициализацию приложения:
	if (!InitInstance(hInstance, nCmdShow))
	{
		return FALSE;
	}

	HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WINDOWSPROJECT1));

	MSG msg;

	// Цикл основного сообщения:
	while (GetMessage(&msg, nullptr, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int)msg.wParam;
}



//
//  ФУНКЦИЯ: MyRegisterClass()
//
//  НАЗНАЧЕНИЕ: регистрирует класс окна.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEXW wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style = CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc = WndProc;
	wcex.cbClsExtra = 0;
	wcex.cbWndExtra = 0;
	wcex.hInstance = hInstance;
	wcex.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WINDOWSPROJECT1));
	wcex.hCursor = LoadCursor(nullptr, IDC_ARROW);
	wcex.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wcex.lpszMenuName = MAKEINTRESOURCEW(IDC_WINDOWSPROJECT1);
	wcex.lpszClassName = szWindowClass;
	wcex.hIconSm = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassExW(&wcex);
}

//
//   ФУНКЦИЯ: InitInstance(HINSTANCE, int)
//
//   НАЗНАЧЕНИЕ: сохраняет обработку экземпляра и создает главное окно.
//
//   КОММЕНТАРИИ:
//
//        В данной функции дескриптор экземпляра сохраняется в глобальной переменной, а также
//        создается и выводится на экран главное окно программы.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
	hInst = hInstance; // Сохранить дескриптор экземпляра в глобальной переменной

	HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

	if (!hWnd)
	{
		return FALSE;
	}

	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

	return TRUE;
}

//
//  ФУНКЦИЯ: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  НАЗНАЧЕНИЕ:  обрабатывает сообщения в главном окне.
//
//  WM_COMMAND — обработать меню приложения
//  WM_PAINT — отрисовать главное окно
//  WM_DESTROY — отправить сообщение о выходе и вернуться
//
//

RECT BG;
RECT windowRECT;
int pressButton = NULL;
int topOrBottom = 1;
int speed = 10;

RECT Rect(int left, int top, int right, int bottom)
{
	RECT ARect;
	ARect.left = left;
	ARect.top = top;
	ARect.right = right;
	ARect.bottom = bottom;
	return ARect;
}

void Sprite(HWND hWnd, LPCWSTR Path, RECT ARect, DWORD Param)
{
	PAINTSTRUCT ps;
	HDC hdc;
	BLENDFUNCTION bfn;
	hdc = BeginPaint(hWnd, &ps);

	HBITMAP bmp = (HBITMAP)LoadImage(NULL, Path, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	HDC memdc = CreateCompatibleDC(hdc);
	SelectObject(memdc, bmp);

	bfn.BlendOp = AC_SRC_OVER;
	bfn.BlendFlags = 0;
	bfn.SourceConstantAlpha = 255;
	bfn.AlphaFormat = AC_SRC_ALPHA;
	AlphaBlend(hdc, ARect.left, ARect.top, 149, 150, memdc, 0, 0, 149, 150, bfn);

	//BitBlt(hdc, ARect.left, ARect.top, ARect.right, ARect.bottom, memdc, 0, 0, Param);

	EndPaint(hWnd, &ps);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_COMMAND:
	{
		int wmId = LOWORD(wParam);
		// Разобрать выбор в меню:
		switch (wmId)
		{

		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
	}
	break;
	case WM_PAINT:
	{
		int spritePadding = 86;

		if (pressButton != NULL)
		{
			speed += 1;
			GetClientRect(hWnd, &windowRECT);

			switch (pressButton)
			{
			case VK_UP: {
				BG = Rect(BG.left, BG.top -= speed, BG.right, BG.bottom);
				break;
			}
			case VK_LEFT: {
				BG = Rect(BG.left -= speed, BG.top, BG.right, BG.bottom);
				break;
			}
			case VK_RIGHT: {
				BG = Rect(BG.left += speed, BG.top, BG.right, BG.bottom);
				break;
			}
			case VK_DOWN: {
				BG = Rect(BG.left, BG.top += speed, BG.right, BG.bottom);
				break;
			}
			case VK_BACK: {
				BG = Rect(100, 100, 150, 200);
				break;
			}
			case 123: {
				POINT p;
				GetCursorPos(&p);
				ScreenToClient(hWnd, &p);
				BG = Rect(p.x - 50, p.y - 50, BG.right, BG.bottom);
				break;
			}
			case 124: {
				BG = Rect(BG.left, BG.top + speed * topOrBottom, BG.right, BG.bottom);
				break;
			}
			case 125: {
				BG = Rect(BG.left + speed * topOrBottom, BG.top, BG.right, BG.bottom);
				break;
			}
			}

			if (windowRECT.top > BG.top) {
				BG = Rect(BG.left, windowRECT.top + speed * 4, BG.right, BG.bottom);
				speed = 10;
			}
			else if (windowRECT.left > BG.left) {
				BG = Rect(windowRECT.left + speed * 4, BG.top, BG.right, BG.bottom);
				speed = 10;
			}
			else if (windowRECT.right < (BG.left + spritePadding)) {
				BG = Rect(windowRECT.right - speed * 4 - spritePadding, BG.top, BG.right, BG.bottom);
				speed = 10;
			}
			else if (windowRECT.bottom < BG.top + spritePadding + 16) {
				BG = Rect(BG.left, windowRECT.bottom - speed * 4 - spritePadding - 16, BG.right, BG.bottom);
				speed = 10;
			}

			Sprite(hWnd, L"833.bmp", BG, SRCCOPY);
			pressButton = 1;

		}
		else {
			BG = Rect(100, 100, 150, 200);
			Sprite(hWnd, L"833.bmp", BG, SRCCOPY);
		}

	}
	break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	case WM_KEYDOWN:
	{
		pressButton = wParam;
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	}
	case WM_MOUSEWHEEL: {

		int wheelDelta;
		wheelDelta = GET_WHEEL_DELTA_WPARAM(wParam);

		if (GetKeyState(VK_SHIFT) < 0) {
			pressButton = 125;
		}
		else {
			pressButton = 124;
		}

		if (wheelDelta < 0) {
			topOrBottom = 1;
		}
		else {
			topOrBottom = -1;
		}
		InvalidateRect(hWnd, NULL, TRUE);
		break;
	}
	case WM_MOUSEMOVE:
	{
		if (wParam & MK_LBUTTON)
		{
			pressButton = 123;
			speed = 10;
			InvalidateRect(hWnd, NULL, TRUE);
		}
		break;
	}

	case WM_KEYUP: {
		speed = 10;
		break;
	}
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Обработчик сообщений для окна "О программе".
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
